package com.frankfeng.plexurechallengeapp.infrastructure.model;

public interface OnListFragmentInteractionListener {
    void onListFragmentInteraction(Store item);
    void onBadgeChanged(int num);
    void onListClicked(Store item);
}
