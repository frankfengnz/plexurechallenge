package com.frankfeng.plexurechallengeapp.infrastructure.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Collections;

public class Store {

    @JsonProperty("id") int mId;
    @JsonProperty("name") String mName;
    @JsonProperty("address") String mAddress;
    @JsonProperty("latitude") double mLatitude;
    @JsonProperty("longitude") double mLongitude;
    @JsonProperty("distance") double mDistance;
    @JsonProperty("featureList") ArrayList<String> mFeatureList;
    @JsonIgnore int mIdInternal;
    @JsonIgnore Boolean mFavourite;

    public Store() {}

    public Store(int internal, int i, String n, String a, double la, double ln, double d, String fe, boolean fa) {
        mIdInternal = internal;
        mId = i;
        mName = n;
        mAddress = a;
        mLatitude = la;
        mLongitude = ln;
        mDistance = d;
        mFavourite = fa;
        if (fe != null) {
            ArrayList<String> list = new ArrayList<>();
            if (fe.contains(", ")) {
                String[] all = fe.split(", ");
                Collections.addAll(list, all);
            } else {
                list.add(fe);
            }
            mFeatureList = list;
        }
    }

    public int getId() {
        return mId;
    }

    public int getInternalId() {
        return mIdInternal;
    }

    public String getName() {
        return mName;
    }

    public String getAddress() {
        return mAddress;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public double getDistance() {
        return mDistance;
    }

    public String getFeatureList() {
        String list = "";
        for (String l : mFeatureList) {
            if (list.length() == 0)
                list = l;
            else
                list += ", " + l;
        }
        return list;
    }

    public boolean getFavourite() {
        return mFavourite;
    }

}
