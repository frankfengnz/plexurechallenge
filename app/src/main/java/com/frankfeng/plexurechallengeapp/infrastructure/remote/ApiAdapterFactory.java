package com.frankfeng.plexurechallengeapp.infrastructure.remote;

import android.content.Context;

import com.frankfeng.plexurechallengeapp.BuildConfig;
import com.frankfeng.plexurechallengeapp.infrastructure.model.JsonHelper;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import java.util.concurrent.TimeUnit;

import retrofit.JacksonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

public class ApiAdapterFactory {
    private static String API_URl = "https://bitbucket.org/YahiaRagaePlex/plexure-challenge/raw/449a2452c03961d5d1a094af524148cc345523db/";

    private final Context mContext;
    private ApiAdapter mRestApi;

    public ApiAdapterFactory(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null!!!");
        }
        mContext = context.getApplicationContext();
    }

    public ApiAdapter get(String cmd) {
        if(mRestApi == null) {
            OkHttpClient client = new OkHttpClient() {
                @Override
                public Call newCall(Request request) {
                    return new Call(this, request) {
                        @Override
                        public void cancel() {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    cancelCallFix();
                                }
                            }).start();
                        }
                        private void cancelCallFix() {
                            super.cancel();
                        }
                    };
                }
            };

            client.setConnectTimeout(5, TimeUnit.SECONDS);
            client.setReadTimeout(30, TimeUnit.SECONDS);
            client.setWriteTimeout(30, TimeUnit.SECONDS);
            client.interceptors().add(new ApiRequestInterceptor(mContext));

            // Logs
            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
                loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                client.interceptors().add(loggingInterceptor);
            }

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_URl)
                    .client(client)
                    .addConverterFactory(JacksonConverterFactory.create(JsonHelper.createMapper()))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();

            mRestApi = retrofit.create(ApiAdapter.class);
        }
        return mRestApi;
    }

}
