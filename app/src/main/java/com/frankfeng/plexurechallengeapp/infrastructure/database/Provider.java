package com.frankfeng.plexurechallengeapp.infrastructure.database;

import java.util.List;
import android.text.TextUtils;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.provider.BaseColumns;
import android.content.ContentProvider;
import android.content.UriMatcher;
import android.net.Uri;
import android.util.Log;

import com.frankfeng.plexurechallengeapp.infrastructure.database.tables.StoreTable;

public class Provider extends ContentProvider
{
	private static String TAG = "Provider";

	public static final String AUTHORITY = "com.frankfeng.plexurechallengeapp.infrastructure.database.Provider";
	private DatabaseHelper dbHelper;
	private SQLiteDatabase db;
	private static final int ALL = 0;
	private static final int ONE = 1;

	private static final String[] tables = {StoreTable.NAME};
	private static final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

	static {
		matcher.addURI(AUTHORITY,"*",ALL);
		matcher.addURI(AUTHORITY,"*/#",ONE);
	}

	@Override
	public boolean onCreate() {
		dbHelper = new DatabaseHelper(getContext());
		return true;
	}


	@Override
	public String getType (Uri uri) {
		return null;
	}

	private String buildSelection(Uri uri, String selection) {
		StringBuilder where = new StringBuilder();

		boolean and = false;
		if(!TextUtils.isEmpty(selection))
			and = true;
		switch ( matcher.match(uri) )
		{
			case ALL:
				break;
			case ONE:
				String id = uri.getLastPathSegment();
				where.append(BaseColumns._ID+" = "+id);
				if(and) where.append(" AND ");
				break;
			default:
				throw new IllegalArgumentException("Invalid URI: " + uri);
		}
		if(and) where.append(selection);
		return where.toString();
	}

	public static String getTable(Uri uri) {
		String table;
		switch ( matcher.match(uri) ) {
			case ALL:
				table = uri.getLastPathSegment();
				break;
			case ONE: 
				List<String> bits = uri.getPathSegments();
				table = bits.get(bits.size()-2);
				break;
			default:
				throw new IllegalArgumentException("Invalid URI: "+uri);

		}
		if(contains(tables,table))
			return table;
		throw new IllegalArgumentException("Invalid URI: "+uri);
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, 
		                String[] selectionArgs, String sortOrder) {

		db = dbHelper.getReadableDatabase();
		String table = getTable(uri);
		String where = buildSelection(uri,selection);
		Cursor result = db.query(table, projection, where, selectionArgs, null, null, sortOrder);

		result.setNotificationUri(getContext().getContentResolver(), uri);

		return result;
	}

	@Override
	public Uri insert (Uri uri, ContentValues values) {
		db = dbHelper.getWritableDatabase();
		if(matcher.match(uri) != ALL) {
			throw new IllegalArgumentException("Invalid URI: " + uri);
		}
		String table = getTable(uri);
		long id = db.insertWithOnConflict(table, null, values,
				SQLiteDatabase.CONFLICT_REPLACE);
		if(id == -1 ) {
			Log.d(TAG,"db.insert failed");
			return null;
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return Uri.parse("content://"+AUTHORITY+"/"+table+"/"+id);
	}

	@Override
	public  int update (Uri uri, ContentValues values, String selection, String[] selectionArgs) {

		db = dbHelper.getWritableDatabase();
		int affected = 0;
		try {
			String table = getTable(uri);
			String where = buildSelection(uri, selection);
			affected = db.update(table,values,where,selectionArgs);
			if(affected > 0)
				getContext().getContentResolver().notifyChange(uri,null);
		} catch (Exception ex) {
		}
		return affected;
	}

	@Override
	public int delete (Uri uri, String selection, String[] selectionArgs) {
		db = dbHelper.getWritableDatabase();

		String table = getTable(uri);
		String where = buildSelection(uri,selection);

		int deleted = db.delete(table,where,selectionArgs);

		if(deleted > 0)
			getContext().getContentResolver().notifyChange(uri,null);

		return deleted;
	}

	private static <T> boolean contains(T[] array, T value) {
		for( T entry : array ) {
			if(entry == value) 
				return true;
			else if ( value != null && value.equals(entry) )
				return true;
		}
		return false;
	}

} 