package com.frankfeng.plexurechallengeapp.infrastructure.viewholder;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import com.frankfeng.plexurechallengeapp.R;
import com.frankfeng.plexurechallengeapp.infrastructure.database.tables.StoreTable;
import com.frankfeng.plexurechallengeapp.infrastructure.model.OnListFragmentInteractionListener;
import com.frankfeng.plexurechallengeapp.infrastructure.model.Store;
import com.frankfeng.plexurechallengeapp.infrastructure.shared.Utilities;
import com.frankfeng.plexurechallengeapp.ui.activities.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyStoreViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.item_background) public View mBackground;
    @BindView(R.id.item_name) public TextView mNameView;
    @BindView(R.id.item_address) public TextView mAddressView;
    @BindView(R.id.item_distance) public TextView mDistanceView;
    @BindView(R.id.item_feature_list) public TextView mFeatureListView;
    @BindView(R.id.item_favourite_switch) public Switch mFavouriteSwitchView;
    @BindView(R.id.item_favourite_layout) public View mFavouriteView;
    @BindView(R.id.item_favourite_delete) public Button mFavouriteDeleteView;
    @BindView(R.id.item_google_map) public ImageButton mGoogleMapView;

    private final String mType;
    public final View mView;
    public Store mItem;

    public MyStoreViewHolder(View view, String type, final OnListFragmentInteractionListener listener) {
        super(view);
        mView = view;
        mType = type;
        ButterKnife.bind(this, view);
        if (mType.equals(MainActivity.TYPE_STORE)) {
            mFavouriteView.setVisibility(View.VISIBLE);
            mFavouriteDeleteView.setVisibility(View.GONE);
            mFavouriteSwitchView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isChecked = ((Switch)v).isChecked();
                    StoreTable.setFavourite(mView.getContext(), mItem.getInternalId(), isChecked);
                    int num = Utilities.addBadgeNumber(mView.getContext(), isChecked?1:-1);
                    if (listener != null)
                        listener.onBadgeChanged(num);
                    Log.e("====>", "onCheckedChanged:"+isChecked);
                }
            });
            mGoogleMapView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoGoogleMap(v.getContext());
                }
            });
        } else {
            mFavouriteView.setVisibility(View.GONE);
            mFavouriteDeleteView.setVisibility(View.VISIBLE);
            mFavouriteDeleteView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StoreTable.setFavourite(mView.getContext(), mItem.getInternalId(), false);
                    if (listener != null)
                        listener.onListFragmentInteraction(mItem);
                }
            });
        }
    }

    public static MyStoreViewHolder create(ViewGroup parent, String type, OnListFragmentInteractionListener listener) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_store, parent, false);
        return new MyStoreViewHolder(view, type, listener);
    }

    private void gotoGoogleMap(Context context) {
        try {
            //String url = "https://www.google.com/maps/dir/?api=1";
            //url += "&destination=" + mItem.getLatitude()+", "+mItem.getLongitude();
            //url += "&mode=driving";
            String url = "https://www.google.com/maps/place/"+ mItem.getLatitude()+","+mItem.getLongitude();
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public String toString() {
        return super.toString() + " '" + mNameView.getText() + "'";
    }
}
