package com.frankfeng.plexurechallengeapp.infrastructure.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.frankfeng.plexurechallengeapp.R;
import com.frankfeng.plexurechallengeapp.infrastructure.model.OnListFragmentInteractionListener;
import com.frankfeng.plexurechallengeapp.infrastructure.model.Store;
import com.frankfeng.plexurechallengeapp.infrastructure.shared.Utilities;
import com.frankfeng.plexurechallengeapp.infrastructure.viewholder.MyStoreViewHolder;
import com.frankfeng.plexurechallengeapp.ui.activities.MainActivity;

import java.util.ArrayList;

import butterknife.BindView;

public class MyStoreRecyclerViewAdapter extends RecyclerView.Adapter<MyStoreViewHolder> {

    private final String mType;
    private final ArrayList<Store> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyStoreRecyclerViewAdapter(String type, ArrayList<Store> items, OnListFragmentInteractionListener listener) {
        mType = type;
        mValues = items;
        mListener = listener;
    }

    @Override
    public MyStoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return MyStoreViewHolder.create(parent, mType, mListener);
    }

    @Override
    public void onBindViewHolder(final MyStoreViewHolder holder, int position) {
        if (mValues == null)
            return;
        try {
            holder.mItem = mValues.get(position);
            holder.mNameView.setText(holder.mItem.getName());
            //holder.mAddressView.setText(holder.mItem.getAddress());
            holder.mAddressView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    holder.mAddressView.setText(holder.mItem.getAddress());
                }
            }, 2000);
            holder.mDistanceView.setText(holder.mItem.getDistance()/1000+"km");
            holder.mFeatureListView.setText(holder.mItem.getFeatureList());
            if (holder.mFavouriteSwitchView != null) {
                holder.mFavouriteSwitchView.setChecked(holder.mItem.getFavourite());
            }
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mListener) {
                        mListener.onListClicked(holder.mItem);
                    }
                }
            });
            if (mType.equals(MainActivity.TYPE_STORE)) {
                String dis = Utilities.getPrefValue(holder.mView.getContext(), "distance_boundary");
                if (dis == null) {
                    dis = "80";
                }
                int distance = 80;
                try {
                    distance = Integer.parseInt(dis);
                } catch (Exception ex) {
                }
                distance *= 1000;
                if (holder.mItem.getDistance() > distance) {
                    holder.mBackground.setBackgroundColor(0xffc0c0c0);
                } else {
                    holder.mBackground.setBackgroundColor(0xffffffff);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (mValues == null)
            return 0;
        return mValues.size();
    }

}
