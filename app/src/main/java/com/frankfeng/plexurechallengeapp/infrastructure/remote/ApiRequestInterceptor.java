package com.frankfeng.plexurechallengeapp.infrastructure.remote;

import android.content.Context;
import android.content.Intent;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class ApiRequestInterceptor implements Interceptor {


    private final Context mContext;

    public ApiRequestInterceptor(Context context) {
        mContext = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder requestBuilder = chain.request().newBuilder();

        requestBuilder.addHeader("Device-Type", "Android App");
        String url = chain.request().httpUrl().toString();

        Response response = chain.proceed(requestBuilder.build());
        if (response != null && response.code() == 401) {
            if (mContext != null) {
                Intent intent0 = new Intent("com.frankfeng.plexurechallengeapp.stop");
                mContext.sendBroadcast(intent0);
            }
        }
        return response;
    }
}
