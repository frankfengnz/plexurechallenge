package com.frankfeng.plexurechallengeapp.infrastructure.database.tables;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.net.Uri;
import android.text.Spanned;
import android.util.Log;

import com.frankfeng.plexurechallengeapp.infrastructure.database.DatabaseHelper;
import com.frankfeng.plexurechallengeapp.infrastructure.database.Provider;
import com.frankfeng.plexurechallengeapp.infrastructure.model.Store;

import java.util.ArrayList;
import java.util.List;

import retrofit.Response;

public class StoreTable {
	private static String _ID = BaseColumns._ID;
	public static String NAME = "table_store";
	public static String URI = "content://"+Provider.AUTHORITY+"/" + NAME;
	public static String[] columns = {_ID, "id", "name", "address", "latitude", "longitude", "distance",
			"feature_list", "favourite", "create_date"};

	public static void onCreate(SQLiteDatabase db) {
		String create = "CREATE TABLE IF NOT EXISTS `" + NAME + "` ("
                + _ID + "       INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "id           INTEGER NOT NULL UNIQUE, "
                + "name         TEXT, "
				+ "address      TEXT, "
				+ "latitude     DOUBLE, "
				+ "longitude    DOUBLE, "
				+ "distance     DOUBLE, "
				+ "feature_list TEXT, "
				+ "favourite    BOOLEAN DEFAULT 0, "
                + "create_date  TEXT DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime'))"
		  +");";
		db.execSQL(create);
	}

	public static void onUpgrade(SQLiteDatabase db) {
		db.execSQL("DROP TABLE IF EXISTS`" + NAME + "`;");
		onCreate(db);
	}

	public static void clearTab(SQLiteDatabase db) {
		db.execSQL("DELETE FROM " + NAME + ";");
	}

	private static boolean findStoreById(ArrayList<Store> list, int id) {
		if (list == null)
			return false;
		for (Store l : list) {
			if (l.getId() == id)
				return true;
		}
		return false;
	}

	public static void getDataFromResponse(Context context, Response<List<Store>> response) {
		if (context == null || response == null || response.body() == null)
			return;
		try {
			ArrayList<Store> list = getStores(context, null, null);
			synchronized (StoreTable.class) {
			    /*
				DatabaseHelper dbHelper = new DatabaseHelper(context);
				SQLiteDatabase db = dbHelper.getWritableDatabase();
				StoreTable.clearTab(db);
				db.close();
				dbHelper.close();
				*/
				Uri contentUri;
				for (Store item : response.body()) {
					if (StoreTable.findStoreById(list, item.getId()))
						continue;
					ContentValues data = new ContentValues();
					data.put("id", item.getId());
					data.put("name", item.getName());
					data.put("address", item.getAddress());
					data.put("latitude", item.getLatitude());
					data.put("longitude", item.getLongitude());
					data.put("distance", item.getDistance());
					data.put("feature_list", item.getFeatureList());
					try {
                        contentUri = context.getContentResolver().insert(Uri.parse(StoreTable.URI), data);
                        Log.i("========>"+item.getId(), contentUri.toString());
                    } catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static ArrayList<Store> getStores(Context context, String features, String orderByDistance) {
		String sort = null;
		if (orderByDistance != null)
			sort = "distance " + orderByDistance;
		String selection = null;
		String[] selectionArgs = null;
		if (features != null) {
			//selection = "feature_list LIKE '%"+features+"%'";
			selection = "feature_list LIKE ?";
			selectionArgs = new String[]{"%"+features+"%"};
		}
		return getData(context, null, selection, selectionArgs, sort);
	}

	public static ArrayList<Store> getFavourite(Context context) {
		String selection = "favourite=?";
		String[] selectionArgs = {"1"};
		return getData(context, null, selection, selectionArgs, null);
	}

	public static void setFavourite(Context context, int id, boolean fav) {
		ContentValues cv = new ContentValues();
		cv.put("favourite", fav?1:0);
		StoreTable.update(context, cv, _ID+"="+id);
	}

	private static int update(Context context, ContentValues cv, String where) {
		ContentResolver contentResolver = context.getContentResolver();
		int ret = 0;
		try {
			Uri contentUri = Uri.parse(StoreTable.URI);
			ret = contentResolver.update(contentUri, cv, where, null);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (ret <= 0)
			return 0;
		return ret;
	}

	private static ArrayList<Store> getData(Context context, String[] projection, String selection,
										   String[] selectionArgs, String sortOrder) {
		Uri contentUri = Uri.parse(StoreTable.URI);
		Cursor results = null;
		try {
			synchronized (StoreTable.class) {
				results = context.getContentResolver().query(contentUri, projection, selection, selectionArgs, sortOrder);
				if (results == null) {
					return null;
				}
				if (!results.moveToFirst() || results.getCount() <= 0) {
					results.close();
					results = null;
					return null;
				}
				int count = results.getCount();
				ArrayList<Store> stores = new ArrayList<>();
				for (int i = 0; i < count; i++) {
					int idInternal = results.getInt(results.getColumnIndexOrThrow(_ID));
					int id = results.getInt(results.getColumnIndexOrThrow("id"));
					String name = results.getString(results.getColumnIndexOrThrow("name"));
					String address = results.getString(results.getColumnIndexOrThrow("address"));
					double latitude = results.getDouble(results.getColumnIndexOrThrow("latitude"));
					double longitude = results.getDouble(results.getColumnIndexOrThrow("longitude"));
					double distance = results.getDouble(results.getColumnIndexOrThrow("distance"));
					String feature_list = results.getString(results.getColumnIndexOrThrow("feature_list"));
					boolean favourite = results.getInt(results.getColumnIndexOrThrow("favourite")) > 0;
					Store item = new Store(idInternal, id, name, address, latitude, longitude, distance, feature_list, favourite);
					stores.add(item);
					results.moveToNext();
				}
				return stores;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (results != null && !results.isClosed())
				results.close();
		}
		return null;
	}

	public static boolean matches(Uri uri) {
		return NAME.equals(Provider.getTable(uri));
	}
}	