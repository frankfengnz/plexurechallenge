package com.frankfeng.plexurechallengeapp.infrastructure.remote;

import com.frankfeng.plexurechallengeapp.infrastructure.model.Store;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Multipart;
import rx.Observable;

public interface ApiAdapter {

    @GET("data.json")
    Call<List<Store>> getData();

}
