package com.frankfeng.plexurechallengeapp.infrastructure.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

import com.frankfeng.plexurechallengeapp.infrastructure.database.tables.StoreTable;

public class DatabaseHelper extends SQLiteOpenHelper {
    private final static String TAG = DatabaseHelper.class.getSimpleName();
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "mydb.db";

	public static final String COLUMN_ID = BaseColumns._ID;

	public DatabaseHelper(Context context) {
		super(context,DATABASE_NAME,null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.i(TAG,"Creating new database if not exist.");
		StoreTable.onCreate(db);
    }

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldversion, int newversion) {
		Log.i(TAG,"Upgrading database from v"+oldversion+" to v"+newversion);
		StoreTable.onUpgrade(db);
	}

}