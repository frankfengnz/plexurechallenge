package com.frankfeng.plexurechallengeapp.infrastructure.shared;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Utilities {

    public static void storeBadgeNumber(Context context, int num) {
        SharedPreferences configuration = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = configuration.edit();
        if (num < 0)
            num = 0;
        editor.putInt("badge_number", num);
        editor.apply();
    }

    public static int addBadgeNumber(Context context, int num) {
        SharedPreferences configuration = PreferenceManager.getDefaultSharedPreferences(context);
        int old = configuration.getInt("badge_number", 0);
        SharedPreferences.Editor editor = configuration.edit();
        num += old;
        if (num < 0)
            num = 0;
        editor.putInt("badge_number", num);
        editor.apply();
        return num;
    }

    public static int getBadgeNumber(Context context) {
        SharedPreferences configuration = PreferenceManager.getDefaultSharedPreferences(context);
        int num = configuration.getInt("badge_number", 0);
        if (num < 0)
            num = 0;
        return num;
    }

    public static void storePref(Context context, String key, String value) {
        SharedPreferences configuration = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = configuration.edit();
        editor.putString(key, value);
        editor.apply();
    }
    public static String getPrefValue(Context context, String key) {
        SharedPreferences configuration = PreferenceManager.getDefaultSharedPreferences(context);
        return configuration.getString(key, null);
    }

}
