package com.frankfeng.plexurechallengeapp.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.frankfeng.plexurechallengeapp.R;
import com.frankfeng.plexurechallengeapp.infrastructure.shared.Utilities;

public class SettingsFragment extends Fragment {
    private OnFragmentInteractionListener mListener;
    public EditText mDistanceBoundary;
    public Button mDistanceBoundarySet;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        mDistanceBoundary = view.findViewById(R.id.distance_boundary);
        mDistanceBoundarySet = view.findViewById(R.id.distance_boundary_set);
        String dis = Utilities.getPrefValue(getContext(), "distance_boundary");
        if (dis == null)
            dis = "80";
        mDistanceBoundary.setText(dis);
        mDistanceBoundarySet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String d = mDistanceBoundary.getText().toString();
                double b = 80;
                try {
                    b = Double.parseDouble(d);
                    Toast.makeText(getContext(), "Successful.", Toast.LENGTH_LONG).show();
                } catch (Exception ex) {
                    mDistanceBoundary.setText("80");
                    Toast.makeText(getContext(), "Invalid input.", Toast.LENGTH_LONG).show();
                }
                Utilities.storePref(getContext(), "distance_boundary", b+"");
            }
        });
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        */
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
