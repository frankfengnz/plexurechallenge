package com.frankfeng.plexurechallengeapp.ui.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.frankfeng.plexurechallengeapp.R;
import com.frankfeng.plexurechallengeapp.infrastructure.database.DatabaseHelper;
import com.frankfeng.plexurechallengeapp.infrastructure.database.tables.StoreTable;
import com.frankfeng.plexurechallengeapp.infrastructure.model.Store;
import com.frankfeng.plexurechallengeapp.infrastructure.remote.ApiAdapter;
import com.frankfeng.plexurechallengeapp.infrastructure.remote.ApiAdapterFactory;
import com.frankfeng.plexurechallengeapp.infrastructure.shared.Utilities;
import com.frankfeng.plexurechallengeapp.ui.fragments.SettingsFragment;
import com.frankfeng.plexurechallengeapp.ui.fragments.StoreFragment;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity  implements SwipeRefreshLayout.OnRefreshListener{
    public final static String TYPE_STORE = "TYPE_STORE";
    public final static String TYPE_FAVOURITE = "TYPE_FAVOURITE";
    @Nullable
    @BindView(R.id.swipe_layout) public SwipeRefreshLayout mSwipeRefreshLayout;
    @Nullable
    @BindView(R.id.nav_view) public BottomNavigationView mNavView;
    private Fragment mStoreFragment;
    private Fragment mFavouriteFragment;
    private Fragment mSettingsFragment;
    private Fragment mCurrentFragment;
    private TextView mBadgeView;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mContext = this;

        //BottomNavigationView navView = findViewById(R.id.nav_view);
        mNavView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        new DatabaseHelper(this);
        mStoreFragment = new StoreFragment();
        ((StoreFragment) mStoreFragment).setType(TYPE_STORE);
        mFavouriteFragment = new StoreFragment();
        ((StoreFragment) mFavouriteFragment).setType(TYPE_FAVOURITE);
        mSettingsFragment = new SettingsFragment();
        getDataFromSource();
        if (mSwipeRefreshLayout != null)
            mSwipeRefreshLayout.setOnRefreshListener(this);
        addBadge();
    }

    private void addBadge() {
        BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) mNavView.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(1);
        BottomNavigationItemView itemView = (BottomNavigationItemView)v;

        View badge = LayoutInflater.from(this).inflate(R.layout.badge, bottomNavigationMenuView, false);
        mBadgeView = badge.findViewById(R.id.notification_badge);
        itemView.addView(badge);
        int num = Utilities.getBadgeNumber(mContext);
        setBadge(num);
    }

    public void setBadge(int num) {
        if (num <= 0) {
            clearBadge();
            return;
        }
        mBadgeView.setVisibility(View.VISIBLE);
        if (num >= 100)
            mBadgeView.setText("99+");
        else
            mBadgeView.setText(num+"");
    }

    private void clearBadge() {
        mBadgeView.setVisibility(View.GONE);
    }

    private void getDataFromSource() {
        try {
            ApiAdapterFactory api = new ApiAdapterFactory(this);
            ApiAdapter apiAdapter = api.get("");

            Call<List<Store>> call = apiAdapter.getData();
            call.enqueue(new Callback<List<Store>>() {
                @Override
                public void onResponse(Response<List<Store>> response, Retrofit retrofit) {
                    Log.d("","onResponse");
                    StoreTable.getDataFromResponse(mContext, response);
                    setFragment(mStoreFragment);
                }

                @Override
                public void onFailure(Throwable t) {
                    Log.e("","onFailure");

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setFragment(Fragment fg){
        if (fg == null)
            return;
        try {
            FragmentManager fm = getFragmentManager();
            Fragment fragment = fm.findFragmentById(R.id.fragment_container);
            if (fragment == null) {
                fm.beginTransaction()
                        .add(R.id.fragment_container, fg)
                        .commit();
            } else {
                fm.beginTransaction()
                        .replace(R.id.fragment_container, fg)
                        .commit();
            }
        } catch (Exception ex) {
            Log.e("", "", ex);
        }
        mCurrentFragment = fg;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_store:
                    setFragment(mStoreFragment);
                    return true;
                case R.id.navigation_favorite:
                    clearBadge();
                    Utilities.storeBadgeNumber(mContext, 0);
                    setFragment(mFavouriteFragment);
                    return true;
                case R.id.navigation_settings:
                    setFragment(mSettingsFragment);
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onRefresh() {
        if (mCurrentFragment != null) {
            if (mCurrentFragment instanceof StoreFragment)
                ((StoreFragment)mCurrentFragment).refresh(true);
        }
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });
        }
    }
}
