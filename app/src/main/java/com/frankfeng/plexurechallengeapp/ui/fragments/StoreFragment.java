package com.frankfeng.plexurechallengeapp.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.frankfeng.plexurechallengeapp.R;
import com.frankfeng.plexurechallengeapp.infrastructure.adapters.MyStoreRecyclerViewAdapter;
import com.frankfeng.plexurechallengeapp.infrastructure.database.tables.StoreTable;
import com.frankfeng.plexurechallengeapp.infrastructure.model.OnListFragmentInteractionListener;
import com.frankfeng.plexurechallengeapp.infrastructure.model.Store;
import com.frankfeng.plexurechallengeapp.ui.activities.MainActivity;

import java.util.ArrayList;

public class StoreFragment extends Fragment implements OnListFragmentInteractionListener, PopupMenu.OnMenuItemClickListener {
    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private String mType = MainActivity.TYPE_STORE;
    private Menu mMenu;
    private PopupMenu mPopupDistance, mPopupFeatures;
    private String mSortByDistance, mFilterFeatures;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public StoreFragment() {
    }

    public static StoreFragment newInstance(int columnCount) {
        StoreFragment fragment = new StoreFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    public void setType(String type) {
        mType =  type;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
        mContext = this.getContext();
        setHasOptionsMenu(true);
    }

    private void createPopupMenuDistance() {
        if (mPopupDistance == null) {
            mPopupDistance = new PopupMenu(mContext, mMenuItemDistance);
            //mMenu = mPopupDistance.getMenu();
            mPopupDistance.setOnMenuItemClickListener(this);

            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.distance_filter, mPopupDistance.getMenu());
        }

    }

    private void createPopupMenuFeatures() {
        if (mPopupFeatures == null) {
            mPopupFeatures = new PopupMenu(mContext, mMenuItemFeatures);
            mPopupFeatures.setOnMenuItemClickListener(this);

            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.feature_filter, mPopupFeatures.getMenu());
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPopupFeatures = null;
        mPopupDistance = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view0 = inflater.inflate(R.layout.fragment_store_list, container, false);

        // Set the adapter
        View view = view0.findViewById(R.id.list);
        View message = view0.findViewById(R.id.message);
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                recyclerView.setLayoutManager(layoutManager);
                DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                        layoutManager.getOrientation());
                recyclerView.addItemDecoration(dividerItemDecoration);
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            ArrayList<Store> stores = null;
            if (mType.equals(MainActivity.TYPE_STORE))
                stores = StoreTable.getStores(this.getContext(), null, null);
            else if (mType.equals(MainActivity.TYPE_FAVOURITE))
                stores = StoreTable.getFavourite(this.getContext());
            recyclerView.setAdapter(new MyStoreRecyclerViewAdapter(mType, stores, mListener));
            if (stores == null) {
                recyclerView.setVisibility(View.GONE);
                message.setVisibility(View.VISIBLE);
            } else {
                recyclerView.setVisibility(View.VISIBLE);
                message.setVisibility(View.GONE);
            }
            mRecyclerView = recyclerView;
        }
        return view0;
    }

    public void refresh(boolean flag) {
        if (mRecyclerView == null)
            return;
        ArrayList<Store> stores;
        if (mType.equals(MainActivity.TYPE_STORE))
            stores = StoreTable.getStores(this.getContext(), mFilterFeatures, mSortByDistance); //ASC DESC
        else if (mType.equals(MainActivity.TYPE_FAVOURITE)) {
            if (!flag)
                return;
            stores = StoreTable.getFavourite(this.getContext());
        } else
            return;
        mRecyclerView.setAdapter(new MyStoreRecyclerViewAdapter(mType, stores, mListener));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
        }
        */
        mListener = (OnListFragmentInteractionListener) this;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    private View mMenuItemDistance, mMenuItemFeatures;
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        mMenu = menu;
        inflater.inflate(R.menu.filter_menu, menu);
        //mMenuItemDistance = menu.findItem(R.id.menu_item_distance);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                mMenuItemDistance = getActivity().findViewById(R.id.menu_item_distance);
                mMenuItemFeatures = getActivity().findViewById(R.id.menu_item_features);
                if (mType.equals(MainActivity.TYPE_STORE)) {
                    createPopupMenuDistance();
                    createPopupMenuFeatures();
                } else {
                    mMenuItemDistance.setVisibility(View.GONE);
                    mMenuItemFeatures.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_features:
                if (mType.equals(MainActivity.TYPE_STORE))
                    mPopupFeatures.show();
                return true;
            case R.id.menu_item_distance:
                if (mType.equals(MainActivity.TYPE_STORE))
                    mPopupDistance.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.sort_ascending:
                mSortByDistance = "ASC";
                refresh(true);
                return true;
            case R.id.sort_descending:
                mSortByDistance = "DESC";
                refresh(true);
                return true;
            case R.id.sort_none:
                mSortByDistance = null;
                refresh(true);
                return true;
            case R.id.filter_free_wifi:
                mFilterFeatures = "FREE_WIFI";
                refresh(false);
                return true;
            case R.id.filter_drivethr:
                mFilterFeatures = "DRIVETHR";
                refresh(false);
                return true;
            case R.id.filter_mccafe:
                mFilterFeatures = "MCCAFE";
                refresh(false);
                return true;
            case R.id.filter_mcadvent:
                mFilterFeatures = "MCADVENT";
                refresh(false);
                return true;
            case R.id.filter_bp:
                mFilterFeatures = "BP";
                refresh(false);
                return true;
            case R.id.filter_bf:
                mFilterFeatures = "BF";
                refresh(false);
                return true;
            case R.id.filter_table_delivery:
                mFilterFeatures = "TABLE_DELIVERY";
                refresh(false);
                return true;
            case R.id.filter_none:
                mFilterFeatures = null;
                refresh(false);
                return true;
        }
        return false;
    }

    @Override
    public void onListFragmentInteraction(Store item) {
        refresh(true);
    }

    @Override
    public void onBadgeChanged(int num) {
        MainActivity activity = (MainActivity)getActivity();
        activity.setBadge(num);
    }

    @Override
    public void onListClicked(Store item) {
        Toast.makeText(getContext(), item.getName()+" selected.", Toast.LENGTH_SHORT).show();
    }
}
